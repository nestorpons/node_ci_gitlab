const express = require("express");
const app = express();

app.get("/", (request, response) => {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Hello1 world with nodejs.");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);
